<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

Route::group(['prefix' => 'api/recaptcha/v1'], function () {
    Route::post('google-v2','Omidmm\recaptcha\controllers\app\recaptcha\v1\RecaptchaController@recaptchaGoogleV2');
    Route::post('sitekey-v2','Omidmm\recaptcha\controllers\app\recaptcha\v1\RecaptchaController@getSiteKeyV2');
});