<?php
/**
 * Created by PhpStorm.
 * User: omid-loptop
 * Date: 10/31/2019
 * Time: 3:41 PM
 */

namespace Omidmm\recaptcha\classes\recaptcha;

use omidmm\recaptcha\models\Settings as SettingsModel;

class Recaptcha
{
    protected $secretKey_v2;
    protected $accessKey;
    protected $setting;
    protected $siteKey_v2;

    public function init()
    {
        $this->setting = SettingsModel::instance();
        $this->secretKey_v2 = $this->setting->secret_key_v2;
        $this->siteKey_v2 = $this->setting->site_key_v2;
    }

    public function getSiteKeyv2(){
        $this->init();
        return $this->siteKey_v2;
    }

    public function recaptchaGoogleV2()
    {
        $this->init();
        $accessKey = input('captcha_token');
        if ($accessKey && $this->secretKey) {
            return response()->json($this->getV2Captcha($accessKey,$this->secretKey_v2));
        }
        return response()->json(['message' => 'failed request'], 404);
    }

    protected function getV2Captcha($accessKey, $secretkey)
    {
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secretkey . "&response={$accessKey}");
        $result = json_decode($response);
        return $result;
    }
}