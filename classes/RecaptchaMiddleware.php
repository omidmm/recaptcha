<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

namespace Omidmm\recaptcha\classes;


use October\Rain\Exception\AjaxException;
use omidmm\recaptcha\models\Settings;
class RecaptchaMiddleware
{
    /**
     * Run the request filter
     *
     *@param \Illuminate\Http\Request $request
     *@param \Closure    $next
     *@return mixed
     */
    public function handle($request, \Closure $next)
    {
        if($request->exists('v2-recaptcha-response')){
            $recaptcha = new Recaptcha(Settings::get('secret_key_v2'));
            $response = $recaptcha->verify(
                $request->input('v2-recaptcha-response'),
                $request->ip()
            );
            /**
             * Fail, if the response isn't OK
             */
            if (! $response->isSuccess()) {
                if ($request->ajax()) {
                    throw new AjaxException( $response->getErrorCodes() );
                } else {
                    foreach ($response->getErrorCodes() as $code) {
                        Flash::error( $code );
                    }

                    return redirect()->back()->withInput();
                }
            }
        }
        if($request->exists('v3-recaptcha-response')){
            $recaptcha = new Recaptcha(Settings::get('secret_key_v3'));
            $response = $recaptcha->verify(
                $request->input('v3-recaptcha-response'),
                $request->ip()
            );
            /**
             * Fail, if the response isn't OK
             */
            if (! $response->isSuccess()) {
                if ($request->ajax()) {
                    throw new AjaxException( $response->getErrorCodes() );
                } else {
                    foreach ($response->getErrorCodes() as $code) {
                        Flash::error( $code );
                    }

                    return redirect()->back()->withInput();
                }
            }
        }

        /**
         * Handle request
         */
        return $next($request);

    }

}