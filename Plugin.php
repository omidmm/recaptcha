<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

namespace omidmm\recaptcha;


use System\Classes\PluginBase;
/*
 *  reCaptcha Plugin Information File
 */
class Plugin extends PluginBase
{
    /*
     * Returns information about this plugin
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'recaptcha',
            'description' => 'Integrates Googles recaptcha  v2 and v3  with October cms',
            'author' => 'Omidmm',
            'icon' => 'icon-key'
        ];
    }
    /*
     * Register settings for this plugin
     *
     * @return array
     */

    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'Recaptcha',
                'description' => 'Manage reCaptcha Api key and settings',
                'icon' => 'icon-key',
                'class' => 'omidmm\recaptcha\models\Settings',
                'keywords'=> 'captcha , recaptcha ',
                'permissions'=>['omidmm.recaptcha.access_setting'],
            ]
        ];
    }

    public function boot()
    {
       $this->app['Illuminate\Contracts\Http\Kernel']
            ->prependMiddleware('omidmm\recaptcha\classes\RecaptchaMiddleware');
    }
    /*
     * Register all components of this plugin
     *
     * @return  array
     */
    public function registerComponents()
    {
        return [
            'Omidmm\recaptcha\components\RecaptchaV2' => 'recaptcha_v2',
           'Omidmm\recaptcha\components\RecaptchaV3' => 'recaptcha_v3'
        ];
    }
    /*
     * Register all permission for this plugin
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'omidmm.recaptcha.access_settings' => [ 'tab' => 'recaptcha' , 'label' => 'Access settings']
        ];
    }

}