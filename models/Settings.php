<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

namespace omidmm\recaptcha\models;

use Model;
/*
 * setting model for store all fields in db
 */

class Settings extends  Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public  $settingsCode = 'omidmm_recaptcha_settings';
    public $settingsFields = 'fields.yaml';


}