<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

namespace omidmm\recaptcha\components;


use Cms\Classes\ComponentBase;
use omidmm\recaptcha\models\Settings;

class RecaptchaV3 extends ComponentBase
{
    /*
     * Setting instance
     * @var \Omidmm\recaptcha\models\Settings
     */
    public $settings;

    /*
     * Return details about this component
     *
     * @return array
     */

    public function componentDetails()
    {
        return [
            'name' => 'Recaptcha v3 google components',
            'description' => 'Display Captcha v3 widget'
        ];

    }

    public function defineProperties()
    {
      return [];
    }
    /*
     * Prepare variables for widget rendering
     *
     */
    public function onRun()
    {
        $this->settings = $this->page['settings'] = Settings::instance();
    }
}