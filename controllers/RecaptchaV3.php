<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

namespace Omidmm\recaptcha\controllers;


use Cms\Classes\Controller;
use omidmm\recaptcha\models\Settings;

class RecaptchaV3 extends Controller
{

  /*
   *  Return property recaptcha as json
   */
    public function settings()
    {
        $settings = Settings::instance();
      return response()->json(['site_key_v3' =>$settings->site_key_v3,'lang'=>$settings->lang]) ;
    }
}