<?php
/**
 * Created by Webkar company
 * programmer : Omidmm
 * website: https://webkar.net
 */

namespace Omidmm\recaptcha\controllers;


use Cms\Classes\Controller;
use omidmm\recaptcha\models\Settings;

class RecaptchaV2 extends Controller
{
    /*
     * Return setting of recaptcha v2 property as json
     */
    public function settings()
    {
        $settings = Settings::instance();
        return response()->json(['site_key_v2' => $settings->site_key_v2 , 'lang'=> $settings->lang]);
   }
}