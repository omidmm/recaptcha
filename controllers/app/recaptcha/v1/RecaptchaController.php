<?php
/**
 * Created by PhpStorm.
 * User: omid-loptop
 * Date: 7/3/2019
 * Time: 12:24 PM
 */

namespace Omidmm\recaptcha\controllers\app\recaptcha\v1;


use Cms\Classes\Controller;
use Omidmm\recaptcha\classes\recaptcha\Recaptcha;
class RecaptchaController extends Controller
{
    /**
     * @param {captcha_token}
     * @return \Illuminate\Http\JsonResponse
     */
    public function recaptchaGoogleV2()
    {
        $recaptcha = new Recaptcha();
      return $recaptcha->recaptchaGoogleV2();
    }
    public function getSiteKeyV2(){
        $recaptcha = new Recaptcha();
        return response()->json(['site_key_v2'=>$recaptcha->getSiteKeyv2()],200);
    }


}